﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class desktopContext : MonoBehaviour {

    public AudioSource soundSrc;
	
	// Update is called once per frame
	void Update () {
        if(Input.GetKeyDown(KeyCode.Escape)){
            SceneManager.LoadScene(0);
        }
	}

    public void filesDeleted(){
        PlayerPrefs.SetInt("bmoStateEnter", 1);
        SceneManager.LoadScene(0);
    }

    public void clickTrigg(){
        soundSrc.Play();
    }
}
