﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class videoMenu : menuController {

    public VideoClip[] clips;
    public VideoPlayer videoPlayer;
    private Canvas canvas;
    private bool play;

	private void Awake()
	{
        canvas = this.gameObject.GetComponent<Canvas>();
        play = false;
	}

	public override void Behaviour(){


        if(play){
            canvas.enabled = false;
            videoPlayer.clip = clips[this.getCurrOpt()];
            videoPlayer.enabled = true;
            videoPlayer.Play();
            play = false;
        }

        if (videoPlayer.enabled && !videoPlayer.isPlaying)
        {
            stopVideo();
        }
    }

    public void startVideo(){
        play = true;
    }

    private void stopVideo(){
        videoPlayer.Stop();
        videoPlayer.enabled = false;
        canvas.enabled = true;
        //play = false;
    }


}
