﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class terminalManager : MonoBehaviour {

    public Text[] texts;
    public string[] phrases;
    public AudioSource sound;
    public GameObject canvas;
    private bool printing;
    private bool next;
    private int num;

	// Use this for initialization
	void Start () {
        next = true;
        num = 0;
        printing = false;
	}
	
	// Update is called once per frame
	void Update () {
        if(next && num < 4){
            triggerNextText(true);
        }

        if(!printing){
            sound.Stop();
        }

        if(num == 4 && !printing && next){
            canvas.GetComponent<Animator>().enabled = true;
            texts[num].enabled = true;
            next = false;
            StartCoroutine(loadCount(texts[num]));
        }

        if(next && num < 7){
            triggerNextText(false);
        }

        if(next && num == 7){
            SceneManager.LoadScene(7);
        }

	}

    private void triggerNextText(bool firstPhase){
        next = false;
        if (firstPhase)
        {
            StartCoroutine(TypeSentence(phrases[num], texts[num]));
        }else
        {
            StartCoroutine(TypeSentence(phrases[num-1], texts[num]));
        }
    }

    IEnumerator TypeSentence(string sentence, Text dialogueText)
    {
        printing = true;
        sound.Play();
        dialogueText.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            dialogueText.text += letter;
            yield return new WaitForSeconds(0.04f);
        }
        printing = false;
        StartCoroutine(sleeper());

    }

    IEnumerator loadCount(Text loadNumber){
        printing = true;
        loadNumber.text = "0%";
        int count = 0;
        while (count <= 100){
            loadNumber.text = count + "%";
            count++;
            yield return new WaitForSeconds(0.02f);
        }
        printing = false;
        StartCoroutine(sleeper());

    }

    IEnumerator sleeper(){
        yield return new WaitForSeconds(1);
        num++;
        next = true;

    }
}
