﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class gameSelect : MonoBehaviour {

    private Animator anim;
    private int mainOpt;
    private int gameOpt;
    private Text[] selections = new Text[2];

	// Use this for initialization
	void Start () {
        anim = this.gameObject.GetComponent<Animator>();
        mainOpt = 0;
        gameOpt = 6;

        selections[0] = GameObject.Find("play").transform.Find("Text").gameObject.GetComponent<Text>();
        selections[1] = GameObject.Find("back").transform.Find("Text").gameObject.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {


        if(Input.GetKeyDown(KeyCode.LeftArrow)){
            anim.SetInteger("transition", 1);
            if(gameOpt > 0){
                gameOpt--;
            }
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            anim.SetInteger("transition", 2);
            if (gameOpt < 8)
            {
                gameOpt++;
            }
        }



        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (mainOpt < 1)
            {
                mainOpt++;
                updateOpt();
            }


        }
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (mainOpt > 0)
            {
                mainOpt--;
                updateOpt();
            }
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            SceneManager.LoadScene(0);
        }
        if (Input.GetKeyDown(KeyCode.Z) || Input.GetKeyDown(KeyCode.Space))
        {
            if (mainOpt == 1){
                SceneManager.LoadScene(1);
            }else{
                UnityEngine.Debug.Log("load game:" + gameOpt);
            }
        }
	}

    void updateOpt()
    {
        Color32 pale = new Color32(88, 227, 121, 255);
        Color32 selec = new Color32(20, 51, 43, 255);

        switch (mainOpt)
        {
            case 0:
                selections[0].color = selec;
                selections[1].color = pale;
                break;
            case 1:
                selections[0].color = pale;
                selections[1].color = selec;
                break;
        }

    }


    public void triggerGame()
    {
        Process proc1 = new Process();

        switch (gameOpt)
        {
            case 3:
                proc1.StartInfo.FileName = "C:/Users/Juampi/Documents/bMOOs/undtl.bat";
                break;
            case 0:
                proc1.StartInfo.FileName = "C:/Users/Juampi/Documents/bMOOs/cuphd.bat";
                break;
            case 1:
                proc1.StartInfo.FileName = "C:/Users/Juampi/Documents/bMOOs/downwell.bat";
                break;
            case 4:
                proc1.StartInfo.FileName = "C:/Users/Juampi/Documents/bMOOs/wizofleg.bat";
                break;
            case 5:
                proc1.StartInfo.FileName = "C:/Users/Juampi/Documents/bMOOs/nidhogg2.bat";
                break;
            case 6:
                proc1.StartInfo.FileName = "C:/Users/Juampi/Documents/bMOOs/gots.bat";
                break;
            case 7:
                proc1.StartInfo.FileName = "C:/Users/Juampi/Documents/bMOOs/whatifadvntrtime3d.bat";
                break;
            case 2:
                proc1.StartInfo.FileName = "C:/Users/Juampi/Documents/bMOOs/deadcells.bat";
                break;
            case 8:
                proc1.StartInfo.FileName = "C:/Users/Juampi/Documents/bMOOs/notahero.bat";
                break;
            default:
                proc1.StartInfo.FileName = null;
                break;
        }


        if (proc1.StartInfo.FileName != null)
        {
            //proc1.StartInfo.WorkingDirectory = Application.temporaryCachePath;

            //string output = null;

            proc1.Start();

            //output = proc1.StandardOutput.ReadToEnd();

            proc1.WaitForExit();
        }

    }
}
