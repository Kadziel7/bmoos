﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class generalMenu : menuController {

    public void gameButton()
    {
        SceneManager.LoadScene(2);
    }

    public void configButton()
    {
        SceneManager.LoadScene(3);
    }


    public void videosButton()
    {
        SceneManager.LoadScene(4);
    }
}
