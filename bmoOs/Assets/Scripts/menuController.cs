﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class menuController : MonoBehaviour {
    private int currOpt;
    public Text[] selections;
    private int maxOpts;
    private bool menuOn;
	/*  currMenu 0-> first menu, functions menu
	 *              currOpt 0-> GAMES
	 *                      1-> MUSIC
	 *                      2-> VIDEOS
	 *                      3-> CONFIG
	 * 
	 *  currMenus 1-> games menu
	 * 
	 * 
	 */
	void Start () {
        currOpt = 0;
        maxOpts = selections.Length - 1;
        /*
        selections[0] = GameObject.Find("games").transform.Find("Text").gameObject.GetComponent<Text>();
        selections[1] = GameObject.Find("music").transform.Find("Text").gameObject.GetComponent<Text>();
        selections[2] = GameObject.Find("videos").transform.Find("Text").gameObject.GetComponent<Text>();
        selections[3] = GameObject.Find("config").transform.Find("Text").gameObject.GetComponent<Text>();
        */


	}
	
	// Update is called once per frame
	void Update () {
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                if (currOpt < maxOpts)
                {
                    currOpt++;
                    updateOpt();
                }


            }
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                if (currOpt > 0)
                {
                    currOpt--;
                    updateOpt();
                }
            }
            if(Input.GetKeyDown(KeyCode.X)){
                SceneManager.LoadScene(0);
            }

        Behaviour();

	}

    public virtual void Behaviour()
    { 
    
    }

    void updateOpt(){
        Color32 pale = new Color32(88, 227, 121, 255);
        Color32 selec = new Color32(20, 51, 43, 255);

        foreach( Text text in selections){
           text.color = pale;
        }

        selections[currOpt].color = selec;
          
    }



    public void buttonPressed(){
        switch(currOpt){
            case 0:
                SceneManager.LoadScene(2);
                break;
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
        }
    }

    public int getCurrOpt(){
        return currOpt;
    }

}
