﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class controlManager : MonoBehaviour {
    public AudioClip[] tracks;
    public VideoClip[] vidClips;
    public GameObject audioObject;
    public GameObject camera;
    private List<staticFace> faces;
    private Animator anim;
    private AudioSource audioSrc;
    private VideoPlayer vidSrc;
    private bool glitchingFiles;
    private ShaderEffect_CRT cRT;
    private ShaderEffect_Scanner scanner;
    private ShaderEffect_CorruptedVram corruptedVram;
    private GlitchEffect glitch1;
    private float timeTrigger;

	// Use this for initialization
	void Start () {
        anim = this.gameObject.GetComponent<Animator>();
        audioSrc = audioObject.GetComponent<AudioSource>();
        vidSrc = GameObject.Find("Video Player").GetComponent<VideoPlayer>();
        glitchingFiles = false;

        cRT = camera.GetComponent<ShaderEffect_CRT>();
        scanner = camera.GetComponent<ShaderEffect_Scanner>();
        corruptedVram = camera.GetComponent<ShaderEffect_CorruptedVram>();
        glitch1 = camera.GetComponent<GlitchEffect>();
        int playerState = PlayerPrefs.GetInt("bmoStateEnter");
        PlayerPrefs.SetInt("bmoStateEnter", 0);
        timeTrigger = 0;
        switch(playerState){
            case 1:
                anim.SetInteger("anim", 17);
                break;
            case 2:
                triggerAnim(21); 
                break;
        }
	}
	
	// Update is called once per frame
	void Update () {
            
        if(glitchingFiles){
            if(!cRT.enabled){
                cRT.enabled = true;
                cRT.scanlineWidth = Random.Range(0, 100);
            }else{
                if(!scanner.enabled){
                    scanner.enabled = true;
                    scanner.area = Random.Range(-0.05f, 0);
                }else{
                    if(!corruptedVram.enabled){
                        corruptedVram.enabled = true;
                        corruptedVram.shift = Random.Range(-20, 20);
                    }else {
                        corruptedVram.enabled = false;
                    }
                    scanner.enabled = false;
                }
                cRT.enabled = false;
            }
            glitch1.enabled = true;
        }


        if(timeTrigger > 38){

            int auxAnim = Random.Range(1, 25);
            switch (auxAnim)
            {
                case 10:
                case 9:
                case 17:
                case 21:
                    break;
                default:
                    triggerAnim(auxAnim);
                    break;
            }
            }
            if (Input.GetKeyDown(KeyCode.Z))
            {
                triggerAnim(1);
            }
            if (Input.GetKeyDown(KeyCode.LeftShift))
            {
                triggerAnim(2);
            }
            if (Input.GetKeyDown(KeyCode.C))
            {
                triggerAnim(3);
            }
            if (Input.GetKeyDown(KeyCode.V))
            {
                triggerAnim(4);
            }
            if (Input.GetKeyDown(KeyCode.B))
            {
                triggerAnim(5);
            }
            if (Input.GetKeyDown(KeyCode.N))
            {
                triggerAnim(6);
            }
            if(Input.GetKeyDown(KeyCode.M)){
                triggerAnim(7);
            }
            if (Input.GetKeyDown(KeyCode.A))
            {
                triggerAnim(8);
            }
            if (Input.GetKeyDown(KeyCode.S))
            {
            //triggerAnim(9);
            vidSrc.enabled = true;
            vidSrc.clip = vidClips[0];
            vidSrc.Play();
            }
            if (Input.GetKeyDown(KeyCode.D))
            {
            //triggerAnim(10);
            vidSrc.enabled = true;
            vidSrc.clip = vidClips[1];
            vidSrc.Play();
            }            
            
            if (Input.GetKeyDown(KeyCode.F)){
                triggerAnim(11);
            }

            if (Input.GetKeyDown(KeyCode.G))
            {
                triggerAnim(12);
            }

            if(Input.GetKeyDown(KeyCode.H)){
                triggerAnim(13);
            }

            if (Input.GetKeyDown(KeyCode.J))
            {
                triggerAnim(14);
            }

            if (Input.GetKeyDown(KeyCode.K))
            {
                triggerAnim(15);
            }

            if (Input.GetKeyDown(KeyCode.L))
            {
                triggerAnim(16);
            }

            if(Input.GetKeyDown(KeyCode.Q)){
                triggerAnim(17);
            }

        if(Input.GetKeyDown(KeyCode.E)){
            triggerAnim(18);
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            triggerAnim(19);
        }

        if(Input.GetKeyDown(KeyCode.T)){
            triggerAnim(20);
        }

        if(Input.GetKeyDown(KeyCode.Y)){
            triggerAnim(21);
        }

        if(Input.GetKeyDown(KeyCode.U)){
            triggerAnim(22);
        }

        if(Input.GetKeyDown(KeyCode.I)){
            triggerAnim(23);
        }

        if (Input.GetKeyDown(KeyCode.O))
        {
            triggerAnim(24);
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            triggerAnim(25);
        }


        if(Input.GetKeyDown(KeyCode.Tab)){
            triggerAnim(26);
        }

            if (Input.GetKeyDown(KeyCode.W))
            {
            SceneManager.LoadScene(5);
            }

            if (Input.GetKeyDown(KeyCode.X))
            {
                SceneManager.LoadScene(1);
            }

            if(vidSrc.enabled && !vidSrc.isPlaying)
            {
                vidSrc.Stop();
                vidSrc.enabled = false;
            }
        timeTrigger += Time.deltaTime;
	}

    void triggerAnim(int num){

        anim.SetInteger("anim", num);
        timeTrigger = 0;
        switch(num){
            
            case 9:
            case 10:
                audioSrc.clip = tracks[3];
                break;
            case 13:
                audioSrc.clip = tracks[4];
                break;
            case 18:
                audioSrc.clip = tracks[6];
                break;
            case 19:
                audioSrc.clip = tracks[7];
                break;
            case 20:
                audioSrc.clip = tracks[8];
                break;
            case 21:
                audioSrc.clip = tracks[9];
                break;
            case 26:
                audioSrc.clip = tracks[10];
                break;
            case 1:
            case 2:
            case 3:
                audioSrc.clip = tracks[num-1];
                break;
            default:
                audioSrc.clip = null;
                break;
        }
    }

    public void playSound(){
        if(!audioSrc.isPlaying){
            audioSrc.Play();
        }else{
            audioSrc.Stop();
        }
    }

    public void playVideo(){
        if(!vidSrc.isPlaying){
            vidSrc.enabled = true;
            vidSrc.Play();
        }else{
            vidSrc.Stop();
            vidSrc.enabled = false;
        }
    }

    public void triggerGlitch(int value){
        switch(value){
            case 0:
                break;
            case 1:
                glitchingFiles = true;
                audioSrc.clip = tracks[5];
                audioSrc.Play();
                break;
        }
    }

    public void triggerTerminal(){
        SceneManager.LoadScene(6);
    }


}
