﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class staticFace{

    int id;
    Sprite[] sprites;
    Vector3 facePos;
    Vector3 mouthPos;
    Vector3 rigthEyePos;
    Vector3 leftEyePos;

}
