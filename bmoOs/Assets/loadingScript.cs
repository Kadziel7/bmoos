﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class loadingScript : MonoBehaviour {

    public Canvas canvas;
    public Text text;
    public Animator barAnim;
    public Animator faceAnim;
    public Image bmoLoadingSprite;
    public Sprite[] spriteLoad;
    public AudioSource audio;
    private bool next;
    private int num;


	// Use this for initialization
	void Start () {
        next = true;
        num = 1;
        PlayerPrefs.SetInt("bmoStateEnter", 2);
	}
	
	// Update is called once per frame
	void Update () {

        if(next){
            next = false;
            switch(num){
                case 1:
                    phase1();
                    break;
                case 2:
                    phase2();
                    break;
                case 3:
                    phase3();
                    break;
                case 4:
                    phase4();
                    break;
                case 5:
                    phase5();
                    break;
                case 6:
                    finalPhase();
                    break;
                case 7:
                    PlayerPrefs.SetInt("bmoStateEnter", 2);
                    SceneManager.LoadScene(0);
                    break;
            }
        }

	}

    private void phase1(){
        barAnim.enabled = true;
        StartCoroutine(sleeper());
    }

    void phase2(){
        text.text = "unloading";
        barAnim.SetBool("loading", false);
        StartCoroutine(sleeper());
    }

    void phase3()
    {
        bmoLoadingSprite.sprite = spriteLoad[1];
        text.text = "reloading";
        barAnim.SetBool("loading", true);
        StartCoroutine(sleeper());
    }

    void phase4()
    {
        bmoLoadingSprite.sprite = spriteLoad[0];
        text.text = "counterloading";
        barAnim.SetBool("loading", false);
        StartCoroutine(sleeper());
    }

    void phase5(){
        bmoLoadingSprite.sprite = spriteLoad[2];
        text.text = "frownloading";
        barAnim.SetBool("loading", true);
        StartCoroutine(sleeper());
    }

    void finalPhase(){
        canvas.enabled = false;
        faceAnim.enabled = true;
        audio.Play();
        StartCoroutine(sleeper());
    }

    IEnumerator sleeper(){
        yield return new WaitForSeconds(3);
        next = true;
        num++;
    }
}
